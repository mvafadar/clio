#!/usr/bin/env python
# coding: utf-8

# ## Silva Data Importer
# 
# ### [Link to Silva database: www.arb-silva.de](https://www.arb-silva.de)

# #### Libraries: Biopython, pandas, sqlite3 & ...

# In[ ]:


from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import FeatureLocation
from Bio.Alphabet import IUPAC
from Bio.Seq import Seq
from Bio.SeqIO import FastaIO
from ete3 import NCBITaxa
import pandas as pd
import argparse
import math
import sqlite3
from sqlite3 import Error
import datetime


# #### Initialization

# In[ ]:


fasta_path = "../inputs/SILVA_132_SSURef_Nr99_tax_silva.fasta"
catalog_path = '../inputs/taxmap_embl_ssu_ref_nr99_132.txt'
db_path = '../clio/clio_sqlite.db'
taxonomy_group = "Metazoa"
marker = "SSU"
database = "silva"
indexer = False
all_primers = list()
elements = [
    "record_id", "sequences",
    "kingdom_name", "kingdom",
    "phylum_name", "phylum",
    "class_name", "class",
    "order_name", "order",
    "family_name","family",
    "genus_name", "genus",
    "species_name", "species",
    "taxid", "marker",
    "database", "extra",
    "genbank_accession","bin_uri",
    "import_file"
]
primers = {
}


# #### Command line arguments

# In[ ]:


parser = argparse.ArgumentParser()
parser.add_argument('fasta', help='SILVA Fasta file,                     link: www.arb-silva.de/fileadmin/silva_databases/release_132                    /Exports/SILVA_132_SSURef_Nr99_tax_silva.fasta.gz')
parser.add_argument('catalog', help='SILVA EMBL Taxonomy catalog,                     link: https://www.arb-silva.de/fileadmin/silva_databases/release_132                    /Exports/taxonomy/taxmap_ncbi_ssu_ref_nr99_132.txt.gz')
parser.add_argument('database', help='Path of Clio database')
parser.add_argument('taxa', help='Target Taxonomy name "Capitalized"') #

args = parser.parse_args()

fasta_path = args.fasta
catalog_path = args.catalog
db_path = args.database
taxonomy_group = args.taxa



# #### NCBI Taxonomy Database

# In[ ]:


ncbi = NCBITaxa()
# ncbi.update_taxonomy_database()



# #### SQL Database functions

# In[ ]:


def create_connection(db_file):
    """ create a database connection to a SQLite database """
    try:
        conn = sqlite3.connect(db_file) # RAM: (':memory:')
        print(sqlite3.version)
        return conn
    except Error as e:
        print(e)
    return None


# In[ ]:


def create_table(conn):
    cur = conn.cursor()

    # create the table if it doesn't exist
    cur.execute('CREATE TABLE IF NOT EXISTS barcode ("{}" TEXT,"{}" TEXT,"{}" TEXT,                 "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT,"{}" TEXT, "{}" TEXT,                 "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT,                 "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT,                 "{}" TEXT, "{}" TEXT);'.format(*elements))
    # create the index to query fast
    createSecondaryIndex1 = "CREATE INDEX IF NOT EXISTS                             index_record_id ON barcode(record_id);"
    createSecondaryIndex2 = "CREATE INDEX IF NOT EXISTS                             index_marker ON barcode(marker);"
    createSecondaryIndex3 = "CREATE INDEX IF NOT EXISTS                             index_database ON barcode(database);"
    cur.execute(createSecondaryIndex1)
    cur.execute(createSecondaryIndex2)
    cur.execute(createSecondaryIndex3)

    conn.commit()
    return 0


# In[ ]:


def insert_record(conn, record_dict):
    cur = conn.cursor()

    values_list = [(str(record_dict[element]) if record_dict[element] else "")
                   for element in elements]

    cur.execute('INSERT INTO barcode VALUES ("{}","{}","{}","{}","{}","{}","{}",                "{}","{}","{}","{}","{}","{}","{}","{}","{}","{}","{}","{}","{}",                "{}","{}","{}");'.format(*values_list))
    conn.commit()
    return 0


# In[ ]:


def insert_log(conn,filepath):
    cur = conn.cursor()
    cur.execute('CREATE TABLE IF NOT EXISTS                 import (import_file TEXT, database TEXT, marker TEXT,date DATETIME);')
    cur.execute('INSERT INTO import VALUES ("{}","{}","{}","{}");'.format(
        filepath,database,'all','GETDATE()'))
    conn.commit()


# In[ ]:


def select_taxid(record_id,conn):
    """
    Query Taxid rows in the catalog table
    :param conn: the Connection object
    :return:
    """
    cur = conn.cursor()
    cur.execute("SELECT c6 FROM catalog WHERE c1 = '{}'".format(record_id))
 
    rows = cur.fetchall()
#     conn.close()
    
    if len(rows) > 0: 
        row1 =  rows[0][0]
        return row1
    else:
        print record_id + " has not been found\n"
        return -1
    


# In[ ]:


def record_exist(conn, record_id, database):
    cur = conn.cursor()
    cur.execute("""SELECT '{0}' FROM barcode
    WHERE '{0}' = '{1}' AND database = '{2}'""".format(elements[0],
                                                       record_id, database))
    rows = cur.fetchall()
    if len(rows) > 0:
        return True
    else:
        return False


# #### Convert csv to SQL table with panda

# In[ ]:


cols = ['c1','c2','c3','c4','c5','c6']
df = pd.read_csv(catalog_path,sep="\t")
df.columns = cols
connection = create_connection(db_path)
df.to_sql('catalog', connection, if_exists='replace', index=False)
connection.close()


# #### Data importer

# In[ ]:


counter = 0
lineager = 0
taxider = 0
conn = create_connection(db_path)
print ("started at: " + str(datetime.datetime.now()))
with open(fasta_path, "rU") as handle:
    for seq_record in SeqIO.parse(handle, "fasta"):
        counter += 1
        # CREATE TABLE IF NOT EXIST
        create_table(conn)
        if taxonomy_group in seq_record.description:
            lineager += 1
            record_dict = dict.fromkeys(elements)
            taxonomy = ";tax="
            desc = ""
            if record_exist(conn,seq_record.id,database) and counter:
                continue
            try:
#                 searching record_id in taxonomy catalog
                ncbi_id = seq_record.id.split('.')[0]
                taxid = select_taxid(ncbi_id,conn)
                if taxid != -1:
                    taxider += 1
                    record_dict[elements[0]] = seq_record.id
                    record_dict[elements[1]] = seq_record.seq.back_transcribe()              
                    record_dict[elements[16]] = taxid
                    record_dict[elements[17]]= marker
                    record_dict[elements[18]]= database
                    record_dict[elements[22]]= fasta_path.split('/')[-1]
                    # Taxonomy info
                    lineage = ncbi.get_lineage(taxid)
                    lineage_ranks = ncbi.get_rank(lineage)
                    i = 0
                    for i in range(3,16,2):
                        rank = elements[i]
                        rank_name = elements[i-1]       
                        if rank in lineage_ranks.values():
                            key = lineage_ranks.keys()[
                                lineage_ranks.values().index(rank)]
                            taxonomy_Name = ncbi.get_taxid_translator([
                                key]).values()[0]
                            record_dict[rank] = key
                            record_dict[rank_name] = taxonomy_Name
                else:
                    print "Taxid: " + str(taxid) + " Not found"
                    continue
            except:
                print seq_record.id + " taxid not found"
                continue
            
            insert_record(conn,record_dict)
conn.close()
print ("finished at: " + str(datetime.datetime.now()))
print counter
print lineager
print taxider

