#!/usr/bin/env python
# coding: utf-8

# ## RefSeq Data Importer
# 
# ### [Link to RefSeq database: www.ncbi.nlm.nih.gov/refseq](https://www.ncbi.nlm.nih.gov/refseq)

# In[ ]:


from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna
from Bio.SeqIO import FastaIO
from Bio import GenBank
from ete3 import NCBITaxa
import os
import glob
from itertools import imap
import collections
import argparse
import sqlite3
from sqlite3 import Error
import datetime
import re


# #### Initialization

# In[ ]:


# "bold_chordata_data.xml" # bold_chordata_full.xml
genbank_path = '../inputs/mitochondrion'
db_path = '../clio/clio_sqlite.db'
taxonomy_group = "Metazoa"
database = "refseq"
indexer = False
all_primers = list()
elements = [
    "record_id", "sequences",
    "kingdom_name", "kingdom",
    "phylum_name", "phylum",
    "class_name", "class",
    "order_name", "order",
    "family_name","family",
    "genus_name", "genus",
    "species_name", "species",
    "taxid", "marker",
    "database", "extra",
    "genbank_accession","bin_uri",
    "import_file"
]
# Mitomarkers
markers_verbose = {
    '12S' : ['12S','RNR1','SSU'],
    '16S' : ['16S','16r','LSU'],
    '18S' : ['18S','18r'],
    'COI' : ['CO1','COX1','COXI']
}
primers = {
}


# #### Command line arguments

# In[ ]:


parser = argparse.ArgumentParser()
parser.add_argument('genbank', help='Genbank only gbff directory,                     FTP-link: ftp://ftp.ncbi.nlm.nih.gov/refseq/release/')
parser.add_argument('db', help='Database path') 
parser.add_argument('taxa', help='Target taxonomy group "Capitalized"') #
args = parser.parse_args()

genbank_path = args.genbank
db_path = args.db
taxonomy_group = args.taxa
# marker = args.m



# #### NCBI Taxonomy Database

# In[ ]:


ncbi = NCBITaxa()
# ncbi.update_taxonomy_database()



# #### SQL Database functions

# In[ ]:


def create_connection(db_file):
#     db_file = "genbank_sqlite.db" 
    """ create a database connection to a SQLite database """
    try:
        conn = sqlite3.connect(db_file) # RAM: (':memory:')
        print(sqlite3.version)
        return conn
    except Error as e:
        print(e)
    return None


# In[ ]:


def create_table(conn):
    cur = conn.cursor()
    
        # create the table if it doesn't exist
    cur.execute('CREATE TABLE IF NOT EXISTS barcode ("{}" TEXT,"{}" TEXT,"{}" TEXT,                "{}" TEXT, "{}" TEXT, "{}" TEXT,"{}" TEXT, "{}" TEXT, "{}" TEXT,                 "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT,                "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT,                "{}" TEXT, "{}" TEXT);'.format(*elements))
    # create the index to query fast
    createSecondaryIndex1 = "CREATE INDEX IF NOT EXISTS                             index_record_id ON barcode(record_id);"
    createSecondaryIndex2 = "CREATE INDEX IF NOT EXISTS                             index_marker ON barcode(marker);"
    createSecondaryIndex3 = "CREATE INDEX IF NOT EXISTS                             index_database ON barcode(database);"
    cur.execute(createSecondaryIndex1)
    cur.execute(createSecondaryIndex2)
    cur.execute(createSecondaryIndex3)

    conn.commit()
    return 0


# In[ ]:


def insert_record(conn, record_dict):
    cur = conn.cursor()

    values_list = [(str(record_dict[element]) if record_dict[element] else "")
                   for element in elements]

    cur.execute('INSERT INTO barcode VALUES ("{}","{}","{}","{}","{}","{}","{}",                "{}","{}","{}","{}","{}","{}","{}","{}","{}","{}","{}","{}","{}",                "{}","{}","{}");'.format(*values_list))
    conn.commit()
    return 0


# In[ ]:


def insert_log(conn,filepath):
    cur = conn.cursor()
    cur.execute('CREATE TABLE IF NOT EXISTS                 import (import_file TEXT, database TEXT, marker TEXT,date DATETIME);')
    cur.execute('INSERT INTO import VALUES ("{}","{}","{}","{}");'.format(
        filepath,database,'all','GETDATE()'))
    conn.commit()


# In[ ]:


def record_exist(conn, record_id, database, marker, gene_id):
    cur = conn.cursor()
    cur.execute("SELECT '{0}' FROM barcode WHERE '{0}' = '{1}' AND                 database = '{2}' AND marker = '{3}' AND extra = '{4}'".format(
        elements[0], record_id, database, marker, gene_id))
    rows = cur.fetchall()
    if len(rows) > 0:
        return True
    else:
        return False


# #### Considering all the files in genbank format

# In[ ]:


def files(path, keyword = ''):
    if os.path.exists(path):
        filename = '/' + keyword + '*gbff*'
        return (os.path.abspath(x) for x in glob.iglob(path + filename))


# #### Data importer

# In[ ]:


c = 0
t = 0
markers = set()
print ("started at: " + str(datetime.datetime.now()))
conn = create_connection(db_path)
create_table(conn)
for filepath in files(genbank_path):
    print filepath
    input_handle = open(filepath, "r")
    file_name = filepath.split('/')[-1]
    try:
        for seq_record in SeqIO.parse(input_handle, "genbank"):
            c += 1
            taxonomy_flag = False
            taxid = None
            # Check the taxonomy group
            lineage_list = seq_record.annotations['taxonomy']
            if taxonomy_group in lineage_list:
                taxonomy_flag = True
            else:
                continue
            # Extract Genbank_accession_id
            genbank_accession_id = ""
            comment = seq_record.annotations['comment']
            term = '...\d[0-9]{4}'
            result = re.search(term, comment)
            if result:
                genbank_accession_id = comment[result.start(
                ):result.end()+1].strip()
            else:
                print "regex not found in:" + seq_record.id

            for feature in seq_record.features:            
                marker_flag = False
                marker = ""
                if 'db_xref' in feature.qualifiers:
                    if 'taxon' in feature.qualifiers['db_xref'][0]:
                        taxid = feature.qualifiers['db_xref'][0].split(':')[-1]

                if 'gene' in feature.type and 'gene' in feature.qualifiers                         and 'db_xref' in feature.qualifiers:
                    if 'GeneID' in feature.qualifiers['db_xref'][0]:
                        gene = feature.qualifiers['gene'][0].lower()
                        gene_id = feature.qualifiers['db_xref'][
                            0].lower().split(':')[1]
                        loc = feature.location
                        length = loc.end.position - loc.start.position
                        record_dict = dict.fromkeys(elements)

                        break_flag = False
                        for m in markers_verbose:
                            if m.lower() == gene:
                                marker_flag = True
                                marker = m
                                break
                            else:
                                for mv in markers_verbose[m]:
                                    if mv.lower() in gene and 'coxii' not in gene:
                                        marker_flag = True
                                        break_flag = True
                                        marker = m
                                        break
                                if break_flag == True:
                                    break

                        # Check if the record exist
                        if record_exist(conn, seq_record.id, database, marker, gene_id):
                            break

                        if marker_flag == True and taxonomy_flag == True and taxid != None:
                            t += 1
                            try:
                                lineage = ncbi.get_lineage(taxid)
                                lineage_ranks = ncbi.get_rank(lineage)
                                i = 0
                                for i in range(3, 16, 2):
                                    rank = elements[i]
                                    rank_name = elements[i-1]
                                    if rank in lineage_ranks.values():
                                        key = lineage_ranks.keys(
                                        )[lineage_ranks.values().index(rank)]
                                        taxonomy_Name =                                         ncbi.get_taxid_translator([key]).values()[0]
                                        record_dict[rank] = key
                                        record_dict[rank_name] = taxonomy_Name
                                record_dict[elements[0]] = seq_record.id
                                record_dict[elements[1]] =                                 seq_record.seq[
                                    loc.start.position:loc.end.position].ungap('-')
                                record_dict[elements[16]] = taxid
                                record_dict[elements[17]] = marker
                                record_dict[elements[18]] = database
                                record_dict[elements[19]] = gene
                                record_dict[elements[20]] = genbank_accession_id
                                record_dict[elements[21]] = gene_id
                                record_dict[elements[22]] = filepath.split('/')[-1]
                            except:
                                print "error in:" + str(seq_record.id)
                            insert_record(conn, record_dict)
    except:
        print "error in file: " + filepath
    input_handle.close()
    insert_log(conn, filepath.split('/')[-1])
conn.close()
print str(markers)
print ("finished at: " + str(datetime.datetime.now()))
print c
print t

