#!/usr/bin/env python
# coding: utf-8

# ## Bold Data Importer
# 
# ### [Link to Bold database: www.boldsystems.org](http://www.boldsystems.org)

# #### Libraries: Biopython, pandas, sqlite3 & ...

# In[ ]:


from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import FeatureLocation
from Bio.Alphabet import IUPAC
from Bio.Seq import Seq
from Bio.SeqIO import FastaIO
from ete3 import NCBITaxa
import sys
import os
import glob
from lxml import etree
import pandas as pd
import argparse
import math
import sqlite3
from sqlite3 import Error
import datetime


# #### Initialization

# In[ ]:


xml_path = "../inputs/bold_chordata_full.xml"
bin_path = '../inputs/bold_chordata_bin.txt'
db_path = '../clio/clio_sqlite.db'
taxonomy_group = "Metazoa"
marker = "COI"
database = "bold"
indexer = False
all_primers = list()
elements = [
    "record_id", "sequences", 
    "kingdom_name", "kingdom",
    "phylum_name", "phylum", 
    "class_name", "class",
    "order_name", "order", 
    "family_name", "family",
    "genus_name", "genus",
    "species_name", "species", 
    "taxid", "marker",
    "database", "extra",
    "genbank_accession", "bin_uri",
    "import_file"
]

primers = {
    'M13F': 'GTAAAACGACGGCCAG',
    'M13R': 'CAGGAAACAGCTATGAC',
    'LCO1490': 'GGTCAACAAATCATAAAGATATTGG',
    'HC02198': 'TAAACTTCAGGGTGACCAAAAAATCA',
    'LepF1': 'ATTCAACCAATCATAAAGATATTGG',
    'LepR1': 'TAAACTTCTGGATGTCCAAAAAATCA'
}


# #### Command line arguments

# In[ ]:


parser = argparse.ArgumentParser()
# ,'--bold-xml',action='store_true')
parser.add_argument(
    'xml', help='''Bold complete XML, 
    link: www.boldsystems.org/index.php/TaxBrowser_Home''')
# ,'--bold-bin',action='store_true')
parser.add_argument(
    'bin', help='''Bold BIN Specimens TSV file,
    Link: www.boldsystems.org/index.php/Public_BarcodeIndexNumber_Home''')
parser.add_argument('db', help='DB file path.')
parser.add_argument('taxa', help='Target taxonomy group "Capitalized"')
parser.add_argument('marker', help='The marker. COI is default')

args = parser.parse_args()

xml_path = args.xml
bin_path = args.bin
db_path = args.db
taxonomy_group = args.taxa
marker = args.marker


# #### NCBI Taxonomy Database

# In[ ]:


ncbi = NCBITaxa()
# ncbi.update_taxonomy_database()


# #### SQL Database functions

# In[ ]:


def create_connection(db_file):
    """ create a database connection to a SQLite database """
    try:
        conn = sqlite3.connect(db_file) # RAM: (':memory:')
        print(sqlite3.version)
        return conn
    except Error as e:
        print(e)
    return None


# In[ ]:


def bin_extractor(bin_id,rank,conn):
    taxonomy_list = []

    cur = conn.cursor()
        
    # Create a secondary key on the named column
    global indexer
    if indexer == False:
        createSecondaryIndex = "CREATE INDEX IF NOT EXISTS                                 index_bin_uri ON bin(bin_uri)"
        cur.execute(createSecondaryIndex)
        indexer = True
    
    cur.execute("""
    SELECT * FROM bin 
    WHERE bin_uri = "{}"
    ORDER BY species_name DESC,
    genus_name DESC,
    family_name DESC,
    order_name DESC,
    class_name DESC;
    """.format(bin_id))

    rows = cur.fetchall()
    if len(rows) > 0:
        row = rows[0]
        for i in range(2,7):
            #         for row in rows:            
            if row[i] is not None and row[i] != "":
                taxonomy_list.append(row[i])
            else:
                taxonomy_list.append(None)
    else:
        print bin_id + " has no BIN data"
    return taxonomy_list


# In[ ]:


def create_table(conn):
    cur = conn.cursor()

    # create the table if it doesn't exist
    cur.execute('CREATE TABLE IF NOT EXISTS barcode ("{}" TEXT,"{}" TEXT,"{}" TEXT,                 "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT,"{}" TEXT, "{}" TEXT,                 "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT,                 "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT, "{}" TEXT,                 "{}" TEXT, "{}" TEXT);'.format(*elements))
    # create the index to query fast
    createSecondaryIndex1 = "CREATE INDEX IF NOT EXISTS                             index_record_id ON barcode(record_id);"
    createSecondaryIndex2 = "CREATE INDEX IF NOT EXISTS                             index_marker ON barcode(marker);"
    createSecondaryIndex3 = "CREATE INDEX IF NOT EXISTS                             index_database ON barcode(database);"
    cur.execute(createSecondaryIndex1)
    cur.execute(createSecondaryIndex2)
    cur.execute(createSecondaryIndex3)

    conn.commit()
    return 0


# In[ ]:


def insert_record(conn, record_dict):
    cur = conn.cursor()

    # insert the record
    values_list = [(str(record_dict[element]) if record_dict[element] else "")
                   for element in elements]
    cur.execute('''INSERT INTO barcode VALUES ("{}","{}","{}","{}","{}","{}",
    "{}","{}","{}","{}","{}","{}","{}","{}","{}","{}","{}","{}","{}","{}",
    "{}","{}","{}")'''.format(*values_list))

    conn.commit()
    return 0


# In[ ]:


def insert_log(conn, filepath):
    cur = conn.cursor()
    cur.execute(
        '''CREATE TABLE IF NOT EXISTS import
        (import_file TEXT, database TEXT, marker TEXT,date DATETIME);''')
    cur.execute('INSERT INTO import VALUES ("{}","{}","{}","{}");'.format(
        filepath, database, 'all', 'GETDATE()'))
    conn.commit()


# In[ ]:


def record_exist(conn, record_id, database):
    cur = conn.cursor()
    cur.execute("""SELECT '{0}' FROM barcode
    WHERE '{0}' = '{1}' AND database = '{2}'""".format(elements[0],
                                                       record_id, database))
    rows = cur.fetchall()
    if len(rows) > 0:
        return True
    else:
        return False


# #### Convert a CSV file directly to a database table

# In[ ]:


cols = [elements[21]] + [s for s in elements[4:15:2]]

df = pd.read_csv(bin_path, sep='\t', usecols=cols, keep_default_na=False)

connection = create_connection(db_path)
# connection.text_factory = lambda x: unicode(x, 'utf-8', 'ignore')
df.to_sql('bin', connection, if_exists='replace', index=True, index_label='id')
connection.close()
len(df)


# #### Data importer

# In[ ]:


counter = 0
genbank_ids = 0
taxids = 0
conn = create_connection(db_path)
create_table(conn)
print ("started at: " + str(datetime.datetime.now()))
for event, elem in etree.iterparse(xml_path, events=('end',), tag=('record')):
    break_flag = False
    record_dict = dict.fromkeys(elements)
    counter += 1
    taxonomy = ";tax="
    sequence = ""
    desc = ""
    processid = ""
    record_dict[elements[22]] = xml_path.split('/')[-1]
    # CREATE TABLE IF NOT EXIST
    for e in elem:
        if e.tag in 'processid':  # elements[0]:
            record_dict[elements[0]] = e.text
            if record_exist(conn, record_dict[elements[0]], database):
                break_flag = True
                break

        if e.tag in elements[21]:  # bin_id
            record_dict[elements[21]] = e.text
        if e.tag in 'taxonomy':
            # almost all bold samples are Metazoa
            record_dict[elements[2]] = "Metazoa"
            record_dict[elements[3]] = "33208"
            for rank in e:
                if rank.tag in elements[4]:
                    record_dict[elements[4]] = rank[0][1].text
                if rank.tag in elements[6]:
                    record_dict[elements[6]] = rank[0][1].text
                if rank.tag in elements[8]:
                    record_dict[elements[8]] = rank[0][1].text
                if rank.tag in elements[10]:
                    record_dict[elements[10]] = rank[0][1].text
                if rank.tag in elements[12]:
                    record_dict[elements[12]] = rank[0][1].text
                if rank.tag in elements[14]:
                    record_dict[elements[14]] = rank[0][1].text
        if e.tag in elements[1]:
            if e[0].find('nucleotides') is not None:
                record_dict[elements[1]] = e[0].find(
                    'nucleotides').text.strip("-")
            if e[0].find('genbank_accession') is not None:
                record_dict[elements[20]] = e[0].find('genbank_accession').text
                genbank_ids += 1
        if e.tag in "tracefiles":
            for read in e:
                if marker in read.find('markercode').text:
                    record_dict[elements[17]] = marker
                    record_dict[elements[19]] = read.find('markercode').text
                    break
                else:
                    all_primers.append(read.find('markercode').text)
# primers tag. now disabled
#                 if read.find('seq_primer') is not None \
# and read.find('direction') is not None:
#                     primer = read.find('seq_primer').text
#                     if primer in primers.keys():
#                         if read.find('direction').text == 'F':
#                             i = 10
#                         if read.find('direction').text == 'R':
#                             i = 11
#                         record_dict[elements[i]] = primers[primer]
#                     else:
#                         if primer not in all_primers:
#                             all_primers.append(primer)
    if break_flag == True:
        continue
    # BOLD Taxonomy correction by adding BIN data
    if record_dict[elements[21]]:
        for i in range(4, 16, 2):
            if record_dict[elements[i]] is None:
                bin_uri = record_dict[elements[21]]
                taxonomy_list = bin_extractor(bin_uri, i, conn)
                if len(taxonomy_list) > 0:
                    x = 0
                    for tax in taxonomy_list:
                        if x < (i-4)/2:
                            x += 1
                            continue
                        record_dict[elements[i]] = tax
                        x += 1
                        i += 2
                break
    for t in range(5, 16, 2):
        rank = elements[t]
        rank_name = elements[t-1]
        if record_dict[rank_name]:
            if ncbi.get_name_translator([record_dict[rank_name]]).values():
                taxon = ncbi.get_name_translator(
                    [record_dict[rank_name]]).values()[0][0]
        else:
            taxon = None
        record_dict[rank] = taxon
        t += 1

    reversed_elements = reversed(elements[5:16:2])
    for el in reversed_elements:
        if record_dict[el]:
            record_dict[elements[16]] = record_dict[el]
            taxids += 1
            break
    record_dict[elements[18]] = database
    insert_record(conn, record_dict)
insert_log(conn,xml_path.split('/')[-1])
conn.close()
print ("finished at: " + str(datetime.datetime.now()))
print str(all_primers)
print counter
print genbank_ids
print taxids

